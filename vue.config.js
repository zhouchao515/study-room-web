module.exports = {
    publicPath: './',
    assetsDir: 'static',
    productionSourceMap: false,
    devServer: {
        open: true,
        host: '127.0.0.1',
        port: 8081,
        https: false,
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
    }
};
