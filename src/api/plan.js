import request from './request'

// 新增计划
export function create(data) {
    return request({
        url: '/api/plan/create',
        data: data,
        method: 'post'
    });
}
// 分页查
export function queryByPage(data) {
    return request({
        url: '/api/plan/queryByPage',
        data: data,
        method: 'post'
    });
}
// 更新
export function update(data) {
    return request({
        url: '/api/plan/update',
        data: data,
        method: 'post'
    });
}
// 删除
export function deleteById(id) {
    return request({
        url: '/api/plan/delete/' + id,
        method: 'delete'
    });
}

