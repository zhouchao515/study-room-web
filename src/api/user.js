import request from './request'

// 用户登录接口
export function login(data) {
    return request({
        url: '/api/user/login',
        data: data,
        method: 'post'
    });
}

// 用户注册接口
export function register(data) {
    return request({
        url: '/api/user/register',
        data: data,
        method: 'post'
    });
}

// 用户更新接口
export function update(data) {
    return request({
        url: '/api/user/update',
        data: data,
        method: 'post'
    });
}
// 用户更新积分接口
export function increasePoints(uid,points) {
    return request({
        url: '/api/user/increasePoints?uid=' + uid + '&points=' + points,
        method: 'post'
    });
}
// 用户状态更新接口
export function updateUserStatus(uid) {
    return request({
        url: '/api/user/updateStatus/' + uid,
        method: 'post'
    });
}
// 获取用户排行接口
export function getUserRank() {
    return request({
        url: '/api/user/getUserRank',
        method: 'get'
    });
}

// 用户更新接口
export function getDetail(uid) {
    return request({
        url: '/api/user/detail/' +  uid,
        method: 'post'
    });
}


// 用户搜索房间接口
export function userSearchRoom(data) {
    return request({
        url: '/api/room/queryByPage',
        data: data,
        method: 'post'
    });
}

// 用户创建房间接口
export function userCreateRoom(data) {
    return request({
        url: '/api/room/create',
        data: data,
        method: 'post'
    });
}
// 用户加入房间接口
export function userJoinRoom(data) {
    return request({
        url: '/api/room/join',
        data: data,
        method: 'post'
    });
}

// 用户离家房间接口
export function userLeaveRoom(uid,roomCode) {
    return request({
        url: '/api/room/leave/' + uid + '/' + roomCode,
        method: 'delete'
    });
}

// 房间详情接口
export function roomDetail(roomCode) {
    return request({
        url: '/api/room/detail/' + roomCode,
        method: 'post'
    });
}
// 更新时长接口
export function updateTimes(roomCode,uid) {
    return request({
        url: '/api/room/updateTimes/' + roomCode + '/' + uid,
        method: 'post'
    });
}
// 房间状态更新接口
export function updateRoomStatus(roomCode) {
    return request({
        url: '/api/room/updateStatus/' + roomCode,
        method: 'post'
    });
}
// 兑奖接口
export function getReward(uid) {
    return request({
        url: '/api/user/getReward/' + uid,
        method: 'post'
    });
}



