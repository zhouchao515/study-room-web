import request from './request'



// 创建新日记
export function createDiary(data) {
    return request({
        url: '/api/diary/create',
        data: data,
        method: 'post'
    });
}

// 日记详情
export function diaryDetail(id) {
    return request({
        url: '/api/diary/detail/' + id,
        method: 'post'
    });
}
// 删除日记
export function deleteDiary(id) {
    return request({
        url: '/api/diary/delete/' + id,
        method: 'delete'
    });
}
// 更新日记
export function updateDiary(data) {
    return request({
        url: '/api/diary/update',
        data: data,
        method: 'post'
    });
}
// 分页查询日记
export function queryByPage(data) {
    return request({
        url: '/api/diary/queryByPage',
        data: data,
        method: 'post'
    });
}


