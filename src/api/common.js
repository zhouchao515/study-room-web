import request from './request'

// 头像上传
export function upload(data) {
    return request({
        url: '/api/common/upload',
        data: data,
        method: 'post'
    });
}