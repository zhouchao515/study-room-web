import axios from 'axios';

const service = axios.create({
    // process.env.NODE_ENV === 'development' 来判断是否开发环境
    withCredentials: true,
    timeout: 50000
});
service.defaults.withCredentials = true
export default service;
