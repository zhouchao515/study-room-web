import request from './request'

//管理员登录
export function adminLogin(data) {
    return request({
        url: '/api/admin/login',
        data: data,
        method: 'post'
    });
}
// 管理员更新
export function adminUpdate(data) {
    return request({
        url: '/api/admin/update',
        data: data,
        method: 'post'
    });
}
// 管理员详情
export function adminDetail(uid) {
    return request({
        url: '/api/admin/detail/' + uid,
        method: 'post'
    });
}
// 分页查用户信息
export function queryUserByPage(data) {
    return request({
        url: '/api/admin/queryUserByPage',
        data: data,
        method: 'post'
    });
}
// 分页查留言信息
export function queryCommentByPage(data) {
    return request({
        url: '/api/admin/queryCommentByPage',
        data: data,
        method: 'post'
    });
}
// 分页查房间信息
export function queryRoomByPage(data) {
    return request({
        url: '/api/admin/queryRoomByPage',
        data: data,
        method: 'post'
    });
}
