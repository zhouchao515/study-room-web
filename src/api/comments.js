import request from './request'

// 新增评论
export function create(data) {
    return request({
        url: '/api/comment/create',
        data: data,
        method: 'post'
    });
}

// 分页查
export function queryByPage(data) {
    return request({
        url: '/api/comment/queryByPage',
        data: data,
        method: 'post'
    });
}

// 删除
export function deleteById(id) {
    return request({
        url: '/api/comment/delete/' + id,
        method: 'delete'
    });
}

