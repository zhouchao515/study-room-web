import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import Axios from "axios";
import 'element-ui/lib/theme-chalk/index.css'
Vue.config.productionTip = false
// 设置基准地址
// Axios.defaults.baseURL = 'http://localhost:8888/'
Axios.defaults.baseURL = 'http://localhost:8899/'
Axios.defaults.withCredentials = true
Vue.use(ElementUI)

// 配置NProgress进度条选项  —— 动画效果
NProgress.configure({ ease: 'ease', speed: 500 })

//对路由钩子函数进行设置
//路由进入前
router.beforeEach((to, from , next) => {
  NProgress.start();
  next();
});
//路由进入后
router.afterEach(() => {
  NProgress.done()
})


new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
