import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'welcome',
    component: () => import('@/views/welCome')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: 'login' */ '@/views/user/Login.vue')
  },
  {
    path: '/user',
    component: () => import(/* webpackChunkName: 'index' */ '@/views/user/UserIndex.vue'),
    children:[
      {
        path:"/",
        component: () => import(/* webpackChunkName: 'roomPage' */ '@/views/user/RoomPageView.vue')
      },
      {
        path:"room/:roomCode",
        component: () => import(/* webpackChunkName: 'roomPage' */ '@/views/user/RoomView.vue')
      },
      {
        path:"diary",
        component: () => import(/* webpackChunkName: 'diary' */ '@/views/user/DiaryView.vue')
      },
      {
        path:"diaryPage",
        component: () => import(/* webpackChunkName: 'diaryPage' */ '@/views/user/DiaryPageView.vue')
      },
      {
        path:"comments",
        component: () => import(/* webpackChunkName: 'comments' */ '@/views/user/Comments.vue')
      },
      {
        path:"profile",
        component: () => import(/* webpackChunkName: 'profile' */ '@/views/ProfileView.vue')
      },
      {
        path:"planList",
        component: () => import(/* webpackChunkName: 'planList' */ '@/views/user/PlanListView.vue')
      },
      {
        path:"points",
        component: () => import(/* webpackChunkName: 'points' */ '@/views/user/PointsRankView.vue')
      },
      {
        path:"otherUsers",
        component: () => import(/* webpackChunkName: 'points' */ '@/views/admin/UserPageView.vue')
      },
    ]
  },
  {
    path: '/admin/login',
    component: () => import(/* webpackChunkName: 'login' */ '@/views/admin/Login.vue')
  },
  {
    path: '/admin',
    component: () => import(/* webpackChunkName: 'admin' */ '@/views/admin/AdminIndex.vue'),
    children: [
      {
        path: '/',
        component: () => import(/* webpackChunkName: 'index' */ '@/views/admin/UserPageView.vue')
      },
      {
        path: 'comments',
        component: () => import(/* webpackChunkName: 'login' */ '@/views/admin/CommentPageView.vue')
      },
      {
        path: 'roomPage',
        name: 'roomPage',
        component: () => import(/* webpackChunkName: 'roomPage' */ '@/views/admin/RoomPageView.vue')
      },
      {
        path: 'profile',
        name: 'profile',
        component: () => import(/* webpackChunkName: 'login' */ '@/views/ProfileView.vue')
      },
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
